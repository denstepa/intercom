require './lib/customers_selector.rb'

RSpec.describe CustomersSelector do
  describe '.parse_line' do
    context 'with valid JSON' do
      subject { CustomersSelector.parse_line('{"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}', 1) }
      it { is_expected.to eq({"latitude" => "51.92893", "user_id" => 1, "name" => "Alice Cahill", "longitude" => "-10.27699"}) }
    end

    context 'with invalid JSON' do
      subject { CustomersSelector.parse_line('{"latitude": "51.92893", "user_id": 1 "name": "Alice Cahill", "longitude": "-10.27699"}', 1) }
      it { expect { subject }.to output(/Invalid JSON in file in line 1/).to_stderr }
    end
  end


  describe '.find_in_file_within_range' do
    context 'with valid file and valid range' do
      subject { CustomersSelector.find_in_file_within_range('./spec/test_data/customers.json', 100) }
      it { is_expected.to eq(['Ian Kehoe(4)', 'Nora Dempsey(5)']) }
    end

    context 'with valid file path and invalid json' do
      subject { CustomersSelector.find_in_file_within_range('./spec/test_data/customers_with_empty_line.json', 100) }
      it 'skips invalid or empty lines lines' do
        is_expected.to eq(['Ian Kehoe(4)'])
      end
    end

    context 'with invalid file path' do
      subject { CustomersSelector.find_in_file_within_range('./spec/invalid_file.json', 100) }
      it { expect { subject }.to raise_error('Cannot find file: ./spec/invalid_file.json') }
    end

    context 'with valid file and valid range' do
      subject { CustomersSelector.find_in_file_within_range('./spec/test_data/customers.json', -1) }
      it { expect { subject }.to raise_error('Invalid range: -1') }
    end
  end
end
