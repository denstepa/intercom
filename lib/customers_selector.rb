require 'json'
require './lib/coordinate.rb'

module CustomersSelector
  INTERCOM_OFFICE_COORDS = [53.339428, -6.257664].freeze

  class << self

    def office_location
      Coordinate.new(*INTERCOM_OFFICE_COORDS)
    end

    def parse_line(line, line_number)
      JSON.parse(line)
    rescue JSON::ParserError
      warn "Invalid JSON in file in line #{line_number}"
    end

    def find_in_file_within_range(file_name, range)
      raise "Cannot find file: #{file_name}" unless File.exist?(file_name)
      raise "Invalid range: #{range}" if range <= 0

      File.readlines(file_name).
        map.with_index { |line, line_number| parse_line(line, line_number) }.
        compact.
        select { |user| office_location.distance_to(Coordinate.new(user['latitude'], user['longitude'])) <= range }.
        sort_by { |u| u['user_id'] }.
        map { |user_data| "#{user_data['name']}(#{user_data['user_id']})" }
    end
  end
end
