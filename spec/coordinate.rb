require './lib/coordinate.rb'

RSpec.describe Coordinate do
  describe '#distance_to' do
    let(:berlin_coordinates) { Coordinate.new(52.5200065999999, 13.404953999999975) }
    let(:dublin_coordinates) { Coordinate.new(53.339428, -6.257664) }
    let(:moscow_coordinates) { Coordinate.new(55.755826, 37.6172999) }
    let(:swords_coordinates) { Coordinate.new(53.458974, -6.219898) }

    context 'when locations are far away' do
      subject { berlin_coordinates.distance_to(dublin_coordinates).round }
      it { is_expected.to eq(1317) }
    end

    context 'when locations are nearby' do
      subject { dublin_coordinates.distance_to(swords_coordinates).round(1) }
      it { is_expected.to eq(13.5) }
    end

  end
end
