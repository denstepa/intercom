#!/usr/bin/env ruby

require './lib/customers_selector.rb'

if ARGV[0]
  begin
    CustomersSelector.find_in_file_within_range(ARGV[0], 100).each { |line| puts(line) }
  rescue StandardError => exception
    warn(exception)
  end
else
  warn 'Please specify a file. For example: customers.json'
end
