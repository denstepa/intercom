class Coordinate
  EARTH_RADIUS = 6_371

  attr_accessor :latitude, :longitude

  def initialize(lat, long)
    self.latitude = lat.to_f
    self.longitude = long.to_f
  end

  def latitude_in_rad
    latitude * Math::PI / 180
  end

  def longitude_in_rad
    longitude * Math::PI / 180
  end

  def distance_to(point)
    longitude_diff_in_rad = (longitude_in_rad - point.longitude_in_rad).abs
    fi_1 = latitude_in_rad
    fi_2 = point.latitude_in_rad

    alfa = Math.cos(fi_2) * Math.sin(longitude_diff_in_rad)
    beta = Math.cos(fi_1) * Math.sin(fi_2) - Math.sin(fi_1) * Math.cos(fi_2) * Math.cos(longitude_diff_in_rad)
    sin_sigma = Math.sqrt((alfa**2 + beta**2))
    cos_sigma = Math.sin(fi_1) * Math.sin(fi_2) + Math.cos(fi_1) * Math.cos(fi_2) * Math.cos(longitude_diff_in_rad)
    Math.atan2(sin_sigma, cos_sigma) * EARTH_RADIUS
  end
end
